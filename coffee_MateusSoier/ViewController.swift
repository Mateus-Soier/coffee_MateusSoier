//
//  ViewController.swift
//  Cachorro
//
//  Created by COTEMIG on 22/09/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Coffe:Decodable{
    let message:String
    let status:String
}

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        getNewCoffe()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func recarregarImagem(_ sender: Any) {
        getNewCoffe()
    }
    func getNewCoffe(){
        AF.request("https://coffee.alexflipnote.dev/random.json")
        .responseDecodable(of: Coffe.self){ response in
                if let file = response.value{
                    self.imageView.kf.setImage(with: URL(string: file.message))
                    print(file)
                }
                
            }
    }
}

